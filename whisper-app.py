import datetime
from pytube import YouTube
from moviepy import *
import streamlit as st
import whisper
import tempfile
import os
import subprocess
import time

def transcribe_file(file):
    st.sidebar.success("Transcribing...")
    # with tempfile.NamedTemporaryFile(delete=False) as temp_audio:
        # temp_audio.write(file.read())

    # Get the absolute path of the temporary audio file
    # audio_file_path = os.path.abspath(temp_audio.name)
    start_time = time.time()

    #args = ['whisper', '--language', 'ru', '--model', 'medium', '--temperature', '0.4', '--task', 'transcribe', '-o', './texts', '--', f'./{file.name}']
    args = ['python', 'whisper-diarization/diarize.py', '--language', 'ru', '--whisper-model', 'distil-large-v3', '--device', 'cpu', '--no-stem', '-a', f'./{file.name}']
    res = subprocess.run(args)

    # Засекаем время после выполнения операции
    end_time = time.time()
    # Вычисляем разницу времени для получения времени выполнения операции
    execution_time = end_time - start_time
    # Преобразуем время выполнения в объект datetime
    time_format = datetime.timedelta(seconds=execution_time)
    # Выводим время в формате HH:MM:SS
    print("Время выполнения операции: ", str(time_format))

    st.sidebar.success("Transcription complete")
    # Clean up the temporary file after processing
    # os.remove(audio_file_path)

####################################
st.title("Multi-lingual Transcription using Whisper")

video_url = st.text_input("Enter the YouTube video link")
#model = whisper.load_model("small")

# Download and save the audio from the video
if st.button("Download Audio"):
    if video_url:
        yt = YouTube(video_url)
        video_stream = yt.streams.filter().first()
        valid_file_name = "".join(c if c.isalnum() else "_" for c in yt.title)
        temp_video_path = valid_file_name + ".mp4"
        video_stream.download(filename=temp_video_path)
        
        video_clip = VideoFileClip(temp_video_path)
        audio_file = video_clip.audio
        audio_file.write_audiofile(valid_file_name + ".mp3")
        video_clip.close()
        st.success("Audio downloaded successfully!")
        with open(valid_file_name + ".mp3", 'rb') as file:
            transcribe_file(file)
    else:
        st.warning("Please enter a valid YouTube video link")

audio_file = st.file_uploader("Upload your audio", type=["wav", "mp3", "m4a"])
        
if st.sidebar.button("Transcribe Audio"):
    #st.sidebar.warning("audio_file : " + str(type(audio_file)))
    if audio_file is not None:
        transcribe_file(audio_file)
    else:
        st.sidebar.error("Please upload an audio file.")

if audio_file is not None:
    audio_file.close()